<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\MachineResource;

/**
*Controller Machinner
*recebe uma requisicao POST e retorna um JSON
*/
class MachineController extends Controller
{

    private $bills= array();

    /**
    *Index
    *
    *@param Request $request
    *@return json
    *
    */
    public function index(Request $request)
    {
       
        $requestNumbers = explode(',', $request->input('numero'));

        if(is_null($requestNumbers) || count($requestNumbers) < 1 )
        {
            return response()->json(["error" => "é necessario passar uma array de numeros"]);
        }else{

            $total = intval($request->input('total'));
            $numbers = $requestNumbers;
            $machine = $this->machine($total, $numbers);
            ksort($machine['data']);
            return response()->json(['total' => $total,
                    'data' => $machine['data']]);

        }
    }

    /**
    *Machine
    *A funcao machine e a controladora da maquina a que cordena o que tem que ser feito, chamando o validador e recebendo os dados para retornar para o usuario
    *
    *@param int $total
    *@param int $numeros
    *@return array
    *
    */    
    public function machine(int $total,array $numbers)
    {
        $count = [];
        $result = 0;
        $data = $this->validation($total, $numbers);
        return $data;
    }
    /**
    *Validacao
    *Funcao responsavel pela validacao das notas e retornar o numero de cada nota para a funcao principal
    *
    *@param int $total
    *@param array $numeros
    *@param array $data
    *@param int $resto
    *@return array
    *
    */   
    public function validation(int $total,array $numbers, array $data = null)
    {
        $higherToLower = $this->higherToLower($total, $numbers);

        if($higherToLower['resto'] == 0){
            return $higherToLower;
        }else{
            $lowerTohigher = $this->lowerTohigher($total, $numbers);

            if($lowerTohigher["resto"] == 0)
            {
                return $lowerTohigher;
            }
            if($lowerTohigher["resto"] > $higherToLower["resto"])
            {
                return $higherToLower;
            }else{
                return $lowerTohigher;
            }
        }

    }

    /**
    *Maior para o Menor
    *Funcao responsavel por varrer a array do maior numero para o menor
    *
    *@param int $total
    *@param array $numeros
    *@return array
    *
    */
    public function higherToLower(int $total, array $numbers)
    {
        arsort($numbers);
        foreach ($numbers as $key => $number) {
            $this->bills[$number] = intdiv($total, $number);
            $rest = $total % $number;
            if($rest == 0 ){
                $this->bills['resto'] = $rest;
                return  ["data"=> $this->bills, "resto" => $rest];
            }
            $total = $rest;
        }   
        $this->bills['resto'] = $rest;
        return  ["data"=> $this->bills, "resto" => $rest];
    }

    /**
    *Menor para o maior
    *Funcao respossavel por varrer a array do menor numero para o maior
    *
    *@param int $total
    *@param array $numeros
    *@return array
    *
    */
    public function lowerTohigher(int $total, array $numbers)
    {
        asort($numbers);
        foreach ($numbers as $key => $number) {
            $this->bills[$number] = intdiv($total, $number);
            $rest = $total % $number;
            if($rest == 0 ){
                $this->bills['resto'] = $rest;
                return  ["data"=> $this->bills, "resto" => $rest];
            }
            $total = $rest;
        }   
        $this->bills['resto'] = $rest;
        return  ["data"=> $this->bills, "resto" => $rest];
    }


}
