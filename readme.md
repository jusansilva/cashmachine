Cach Machine
Nesse repositorio você encontrarar uma simples API que recebe um valor inteiro e uma serie de numeros separados por "virgulas" e basicamente ele toma esse valor inteiro como saldo e as series de numeros como notas disponiveis e simula um caixa eletronico de um banco, retornando quanto de cada nota sera necessario para dar o valor.

Instalacao

Clone o Projeto: git clone https://github.com/caouecs/Laravel-lang.git
ou faça Download zip file
Rode o comando composer: composer update -
Rode o comando: php artisan serve
Abra o link proposto pelo terminal no postman (https://www.getpostman.com/downloads/)
escolha o vermo HTTP - POST
adicione a uri - "/api/machine"
na aba "body" selecione "x-www-form-urlencoded"
adicione as seguintes chaves: total e numero

cheve total - na chave total coloque um numero inteiro. ex: 120.
chave numero - na chame numero coleque uma serie de numero inteiros separados por virgulas. ex: 1, 5, 60, 30, 55
 
apos o procedimento aperte o botão "send"

o retorno esperado será:



# Laravel 5.6:
foi utilizado laravel 5.6 https://laravel.com/docs/5.6
